import speech_recognition as sr
import pyttsx3
import requests as req


r = sr.Recognizer()

mic = sr.Microphone(device_index=6)

while True:
    with mic as source:
        print('Speak anything: ')
        # r.adjust_for_ambient_noise(source)
        audio = r.listen(source, timeout=1000)

        try:
            text = r.recognize_google(audio, language='fa-IR')
            print('you said: {}'.format(text))
            if text == 'روشن':
                try:
                    req.get("http://192.168.43.250/on")
                    print('led is on')
                except:
                    print("request failed")
            elif text == 'خاموش':
                try:
                    req.get("http://192.168.43.250/off")
                    print('led is off')
                except:
                    print("request failed")
            else:
                print('there is nothing to do')
        except:
            print('can not recognize voice')